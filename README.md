# Specifications for multi tenant customer area

## General Specs
The application works a customer portal for a loyalty card service. 
A list of shops available for the circuit will be displayed in the home page grouped by categories.
Shops are divided in three groups
* Online Shops
* Network Marketplaces
* City Shops

### Non authenticated users can:
* See the lists of shops for the loyalty circuits, where tey can spend their credit or buying from the shops to gain cashback credits. 
* Registration 

### Authenticated customers can:
* Hit the shop links using a custom referral link with user id passed via query string, so customers can be recognized by the destination shop.
* Check their card balance and movements list (with pagination)
* Modify their personal data
* Being able to generate a referral link to invite a friend

## Dev details
We're looking for a dev team to continue an existing react+nextjs application, we already have a "dummy" prototype but we need to implement actual features interacting with API.
[Working prototype](https://store-app-ten.vercel.app)

## Task list
### Domain based multitenancy
The application will be served from different domains, each domain has a set of static assets (logo, banner and other ui design elements), assets list can be fetched from a REST api (OpenApi standard .yml specs will be provided).
Multitenancy must be implemented on server side using nextjs SSR. 
Assets path should be then passed to the components as props.

### Implement API for the site features
* Login API (using OAUTH2 implicit grant)
* Fetching home page shop and categories boxes (OpenApi specs will be provided)
* Profile update
s* Balance / Movement list API with paginated data (OpenApi specs will be provided)
